package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.sysman;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read.ISysmanResourceReadService;

public class SysmanResourceTest {

	private ISysmanResourceReadService readService;
	
	// @Before
    public void prepare() throws Exception  {
    	//可以加载多个配置文件
        String[] springConfigFiles = {"Dubbo.xml","mybatis-spring-config.xml" };

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
		context.setConfigLocations(springConfigFiles);
		context.refresh();

		readService = (ISysmanResourceReadService) context.getBean( "sysmanResourceService" );
        
    }
    
    // @Test
    public void selectMaxDepthFromSelectedNodeToLeafTest() {
    	List<LinkedHashSet<String>> strList = readService.selectRootToLeafNodePathStrList();
		for (int i = 0; i < strList.size(); i++) {
			System.out.println(strList.get(i));
		}
		
		int maxDepth = readService.selectMaxDepthFromSelectedNodeToLeaf("11048197823472640");
		System.out.println("maxDepth = " + maxDepth);
    }
	
	// @Test
	public void selectRootToLeafNodePathStrListTest() {
		List<LinkedHashSet<String>> strList = readService.selectRootToLeafNodePathStrList();
		for (int i = 0; i < strList.size(); i++) {
			System.out.println(strList.get(i));
		}
	}
	
	//@Test
  	public void selectParentIdsByPrimaryKeyTest() {
  		List<String> list = new ArrayList<String>();
  		list.add("10986709985993728L");
  		List<String> resultList = readService.selectParentIdsByPrimaryKey("10986709985993728", list);
  		for(String _l : resultList) {
  			System.out.println(_l);
  		}
  	}
}
