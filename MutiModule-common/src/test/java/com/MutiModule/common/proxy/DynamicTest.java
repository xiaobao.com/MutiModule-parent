package com.MutiModule.common.proxy;

import java.lang.reflect.InvocationHandler;

import org.junit.Test;

/**
 * 动态代理
 * @author alexgaoyh
 *
 */
public class DynamicTest {
	
	//@Test
	public void dynamicTest() {
		IHelloService real = new HelloServiceImpl();
		InvocationHandler h = new MyInvocationHandler(real);

		// 获得被代理类所实现的所有接口的数组，在这里数组中只有Subject.class一个元素
		Class[] interfaces = real.getClass().getInterfaces();

		// 获得类加载器
		ClassLoader loader = h.getClass().getClassLoader();

		// 获得动态代理类的实例
		Object s = java.lang.reflect.Proxy.newProxyInstance(loader, interfaces, h);

		// 通过代理类对象调用方法
		IHelloService sub = (IHelloService) s;
		sub.sayHello("alexgaoyh");
	}
}
