package com.MutiModule.common.treeNode;

/**
 * 实体类，内含id parentId 属性
 * @author alexgaoyh
 *
 */
public class Resource {

	private Long id;
	
	private Long parentId;
	
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
