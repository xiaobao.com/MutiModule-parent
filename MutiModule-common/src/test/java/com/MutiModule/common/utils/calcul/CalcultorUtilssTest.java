package com.MutiModule.common.utils.calcul;

import org.junit.Test;

public class CalcultorUtilssTest {

	// @Test
	public void testCalcultor() {
		CalculatorUtilss cal = new CalculatorUtilss();
		try {
			cal.exec("4+(3*(3-1)+2)/2"); // = 8
			cal.exec("4 + (-3 * ( 3 - 1 ) + 2)"); // = 0
			cal.exec("4.5+(3.2+3)/2"); // = 7.6
			cal.exec("-4.5+(3.2-3)/2"); // = -4.4
			// cal.exec("4 +-/ (-3 * ( 3 - 1 ) + 2)"); // incorrect expression!
			cal.exec("4.5+(3.2:3)/2"); // incorrect expression!
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
