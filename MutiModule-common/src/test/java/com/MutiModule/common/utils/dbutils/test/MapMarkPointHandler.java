package com.MutiModule.common.utils.dbutils.test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BasicRowProcessor;

public class MapMarkPointHandler extends BasicRowProcessor {

    @Override
    public List toBeanList(ResultSet rs, Class clazz) {
        try {
            List newlist = new LinkedList();
            while (rs.next()) {
                newlist.add(toBean(rs, clazz));
            }
            return newlist;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Object toBean(ResultSet rs, Class type) throws SQLException {
    	
    	MapMarkPointVO _vo = new MapMarkPointVO();
    	_vo.setName(rs.getString("name"));
    	_vo.setValue(rs.getInt("value"));
    	Double[] _obj = new Double[2];
    	_obj[0] = rs.getDouble("lng");
    	_obj[1] = rs.getDouble("lat");
    	_vo.setGeoCoord(_obj);

        return _vo;

    }
}