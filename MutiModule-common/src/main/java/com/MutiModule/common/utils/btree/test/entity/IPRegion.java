package com.MutiModule.common.utils.btree.test.entity;

import com.MutiModule.common.utils.btree.test.util.BTreeUtil;

public class IPRegion implements Comparable<IPRegion> {
	private String beginIP;
	private String endIP;
	private String region;
	
	public IPRegion(String beginIP, String endIP, String region) {
		this.beginIP = beginIP;
		this.endIP = endIP;
		this.region = region;
	}

	public String getBeginIP() {
		return beginIP;
	}

	public void setBeginIP(String beginIP) {
		this.beginIP = beginIP;
	}

	public String getEndIP() {
		return endIP;
	}

	public void setEndIP(String endIP) {
		this.endIP = endIP;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public int compareTo(IPRegion o) {
		if (BTreeUtil.ip2long(this.beginIP) > BTreeUtil.ip2long(o.beginIP)) {
			return -1;
		} else if (BTreeUtil.ip2long(this.beginIP) < BTreeUtil.ip2long(o.beginIP)) {
			return 1;
		} else {
			if (BTreeUtil.ip2long(this.endIP) > BTreeUtil.ip2long(o.endIP)) {
				return 1;
			} else if (BTreeUtil.ip2long(this.endIP) < BTreeUtil.ip2long(o.endIP)) {
				return -1;
			} else {
				return 0;
			}
		}
	}
}