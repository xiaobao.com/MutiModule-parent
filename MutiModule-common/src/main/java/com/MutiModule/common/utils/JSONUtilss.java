package com.MutiModule.common.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;
import org.codehaus.jackson.type.TypeReference;

public class JSONUtilss {

	/**
	 * json 化
	 * @param data
	 * @param response
	 * @throws IOException
	 */
	public static void renderJson(Object data, HttpServletResponse response) throws  IOException {
		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write(object2String(data)) ;
		writer.flush();
	}
	
	/**
	 * json化
	 * @param data
	 * @return
	 * @throws IOException
	 */
	public static String object2String(Object data) throws IOException {
		ObjectMapper om = new ObjectMapper();
		SimpleFilterProvider filterProvider = new SimpleFilterProvider().setFailOnUnknownId(false);
		om.setFilters(filterProvider);
		return om.writeValueAsString(data);
	}
	
	/**
	 * 使用泛型方法，把json字符串转换为相应的JavaBean对象。
	 * (1)转换为普通JavaBean：readValue(json,Student.class)
	 * (2)转换为List,如List<Student>,将第二个参数传递为Student
	 * [].class.然后使用Arrays.asList();方法把得到的数组转换为特定类型的List
	 * 
	 * @param jsonStr
	 * @param valueType
	 * @return
	 */
	public static <T> T readValue(String jsonStr, Class<T> valueType) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(jsonStr, valueType);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * jackson使用问题:json中新增加一个字段,而对象中没有,readvalue会报错额
	 * 解决这个问题  isIgnore 设置为 true 即可
	 * @param jsonStr
	 * @param valueType
	 * @param isIgnore
	 * @return
	 */
	public static <T> T readValue(String jsonStr, Class<T> valueType, boolean isIgnore) {
		ObjectMapper objectMapper = new ObjectMapper();
		if(isIgnore) {
			//设置输入时忽略JSON字符串中存在而Java对象实际没有的属性  
			objectMapper.getDeserializationConfig().set(                  
			    org.codehaus.jackson.map.DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		}
		try {
			return objectMapper.readValue(jsonStr, valueType);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * json数组转List
	 * @param jsonStr
	 * @param valueTypeRef
	 * @return
	 */
	public static <T> T readValue(String jsonStr, TypeReference<T> valueTypeRef){
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			return objectMapper.readValue(jsonStr, valueTypeRef);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 把JavaBean转换为json字符串
	 * 
	 * @param object
	 * @return
	 */
	public static String toJSon(Object object) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(object);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * 把JavaBean转换为json字符串 忽略null值部分
	 * @param object
	 * @return
	 */
	public static String toJSONWithOutNull(Object object) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.setSerializationInclusion(Inclusion.NON_NULL);
			return objectMapper.writeValueAsString(object);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
