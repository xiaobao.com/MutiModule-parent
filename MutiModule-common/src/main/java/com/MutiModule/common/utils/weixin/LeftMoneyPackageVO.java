package com.MutiModule.common.utils.weixin;

import java.math.BigDecimal;

/**
 * 红包实体类信息
 * @author alexgaoyh
 *
 */
public class LeftMoneyPackageVO {
	
	public LeftMoneyPackageVO(int remainSize, BigDecimal remainMoney) {
		this.remainSize = remainSize;
		this.remainMoney = remainMoney;
	}

	/**
	 * 剩余的红包数量
	 */
	private int remainSize;
	
	/**
	 * 剩余的钱
	 */
	private BigDecimal remainMoney;

	public int getRemainSize() {
		return remainSize;
	}

	public void setRemainSize(int remainSize) {
		this.remainSize = remainSize;
	}

	public BigDecimal getRemainMoney() {
		return remainMoney;
	}

	public void setRemainMoney(BigDecimal remainMoney) {
		this.remainMoney = remainMoney;
	}
	
}
