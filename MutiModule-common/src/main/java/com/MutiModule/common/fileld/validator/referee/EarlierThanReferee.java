package com.MutiModule.common.fileld.validator.referee;

import java.util.Date;

import com.MutiModule.common.fileld.validator.AbstractCompareReferee;
import com.MutiModule.common.fileld.validator.Rule.EarlierThan;
import com.MutiModule.common.fileld.validator.State;

/**
 * 日期早于比较
 * 
 * 比较日期与被比较日期需均为@{code java.util.Date}类型, 且不为null 
 * 
 */
public class EarlierThanReferee extends AbstractCompareReferee<EarlierThan> {

	@Override
	public State check(Object data) {
		Object target = getFieldValue(rule.value());
		if (data == null || target == null) {
			return failure(getMessageRuleFirst("datetime.earlier", String.format("Both field<%s> and to-compare-field<%s> cannot be null.", fieldName, rule.value())));
		}
		if (!(data instanceof Date) || !(target instanceof Date)) {
			return failure(getMessageRuleFirst("datetime.earlier", String.format("Both field<%s> and to-compare-field<%s> only allowed type of java.util.Date.", fieldName, rule.value())));
		}
		Date currDate = (Date) data, targetDate = (Date) target;

		if (currDate.getTime() <= targetDate.getTime())
			return simpleSuccess();
		else
			return failure(getMessageRuleFirst("datetime.earlier", "The data is not earlier than target data."));

	}
	
	@Override
	public State check(Object data, String serviceLine) {
		String annotationServiceLine = this.rule.serviceLine();
		if(annotationServiceLine != null && !annotationServiceLine.equals("")) {
			if(annotationServiceLine.contains(serviceLine)) {
				return check(data);
			} 
		} 
		return new State(true, "");
	}

}
