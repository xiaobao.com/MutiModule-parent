package com.MutiModule.common.fileld.validator.referee;

import com.MutiModule.common.fileld.validator.AbstractReferee;
import com.MutiModule.common.fileld.validator.Rule.English;
import com.MutiModule.common.fileld.validator.State;

public class EnglishReferee extends AbstractReferee<English> {

	@Override
	public State check(Object data) {
		return regexMatch("^[A-Za-z\\s\\t\\n\\x0B\\f\\r]+$", data, getMessageRuleFirst("string.english", "The value is not english"));
	}
	
	@Override
	public State check(Object data, String serviceLine) {
		String annotationServiceLine = this.rule.serviceLine();
		if(annotationServiceLine != null && !annotationServiceLine.equals("")) {
			if(annotationServiceLine.contains(serviceLine)) {
				return check(data);
			} 
		} 
		return new State(true, "");
	}
}
