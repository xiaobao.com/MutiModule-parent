#20150701
	增加 MutiModule-common 部分，通用模块，util方法……
	PropertiesUtilss	配置文件讀取util
	
#20150704
	kindeditor 4.1.11 版本；
	将源码包的文件上传和文件空间两个对应的jsp文件改为servlet部分；
		对其中 FileUploadServlet。java 部分进行部分修改，增加 if(item.getName() != null && !item.getName().equals("")){} 部分判断，用来判断是否选择了文件
	
#20150706
	GraphicsMagick+im4java 进行图片处理操作， 需要注意的是，在windows测试过程中，需要修改 imageMagicPath.properties 文件中 imageMagicPath 的制定路径
	linux 环境中，直接打包就可以，不需要修改相关。
	不管是什么操作系统，图片处理功能，都需要安装  ImageMagick http://www.graphicsmagick.org/index.html 
	
#20150709
	增加 DesUtilss 类 ，为 对称加密解密类，其中引入自定义的 BASE64DecoderReplace 类，用来提到(sun.misc.BASE64Decoder类)
	对称加密解密算法，可以用来在CookieUtilss 方法中，对存入的cookie值进行处理，防止客户端改变cookie值进行非法操作。
	
#20150711
	扩展  mybatis-generator-maven-plugin 功能，在自动生成代码时添加分页功能：
		com.MutiModule.common.mybatis.plugin.PaginationPlugin mysql 分页扩展插件
			com.MutiModule.common.vo.mybatis.pagination.Page 分页插件依赖的分页类 
			
		com.MutiModule.common.mybatis.plugin.DeleteLogicByIdsPlugin 自定义的扩展插件
			实现增加一个方法，方法名称为 deleteLogicByIds， 在 接口文件和XML的sql文件中，都增加自定义的方法。
			
#20150713
	增加 XmlParserUtilss dom4j 相关的处理XML 通用方法
	
#20150714
	维护XmlParserUtilss 方法
	增加 生成xml 字符串的方法，详见单元测试部分   GenerateXMLTest
	
#20150715
	XMLUtilss 增加方法，解析xml标签元素
	
#20150717
	com.MutiModule.common.kindeditor.servlet.FileManagerServlet		com.MutiModule.common.kindeditor.servlet.FileUploadServlet
		如上两个类文件，增加不同用户条件下的文件空间的限定，在针对两个servlet进行访问的时候，增加两个入参， 
			 ?contextPath=admin&detailPath=1
			其中第一个参数contextPath用来表明那个项目模块调用这个servlet，是大的目录部分，第二个参数detailPath表明这个模块下的哪一个用户进行的访问
			contextPath： 项目上下文路径，区分项目级别的文件路径
			detailPath: 用户级别路径，用来区分不同用户上传的文件，防止不同用户上传的资料被其他用户看到，如果是未登录用户，使用 anonymous 表示；
			
#20150723
	增加讀取配置文件的類文件，詳見 TestPropertiesUtilss TestPropertiesUtilssTest 兩個文件，
		讀取配置文件，並且將配置文件的內容存在緩存中，只需調用getXXX() 方法即可獲取到配置文件中的值			
			增加sso.properties 文件，將sso單點登錄相關的配置文件讀取功能完成；
			
#20180724
	增加 BinaryTest 的单元测试部分，使用二进制数据的每一位，表示不同的用户信息;
		注意这样的话， 保存的用户信息位数是有限制的，Integer有位数限制，不能随意扩展，如果扩展位数过多，可以考虑使用String类型的数字；
		
#20150922
	RSA 非对称加密算法	com.MutiModule.common.utils.RSA.ConfigTools
	common模块，增加dict 数据字典相关的包结构，后期将大部分常量相关……，之类的数据放置数据字典中；
	
#20151022
	MutiModule-common 模块中，增加 分布式主键ID生成策略的功能（twitter/snowflake），并且完成单元测试，详见  IdWorkerTest.testIdWorkerInstance()	方法；
	需要注意的是，不同的分布式环境下，这里的  idWorker.properties 配置文件的参数需要不同，需要强烈注意，否则主键生成策略会出现重复情况；
	
#20151112
	com.MutiModule.common.mybatis.plugin.serializable.SerializablePlugin
		MyBatis Generator生成DAO 的时候，自定义扩展插件，不仅仅在针对实体类序列化，通用针对 *Example 类序列化
		
#20151121
	com.MutiModule.common.asyncCallback	单元测试，完成java的异步回调机制；
	业务场景可以如下设定：	请求者发送一个http请求，http请求执行时间过长，此时请求者可以接着执行后面的请求，待http请求执行完毕之后，调用回调函数完成返回值的处理逻辑；
	
	java 封装自定义分页组件，通用实体类在 com.MutiModule.common.myPage 包下； 据图使用情况在  MutiModule-Dubbo-RWSeperate-consumer-web MyPageViewController下
		
		int totalCount = 305;
		int recordPerPage = 10;//每页显示条数
		int currentpage = 10;//当前页数

		List<Result> list = new ArrayList<Result>();
		// 模拟数据
		for (int i = 1; i <= totalCount; i++) {
			Result result = Result.ok(i + "");
			list.add(result);
		}

		MyPageView<Result> pageView = new MyPageView<Result>(recordPerPage, currentpage);
		int start = pageView.getStartRecordIndex() > totalCount ? totalCount : pageView.getStartRecordIndex();
		int lenght = pageView.getRecordPerPage();
		
		int end = ((start + lenght) >= totalCount) ? totalCount : (start + lenght);
		
		MyQueryResult<Result> qr = new MyQueryResult<Result>(list.subList(
				start, end), list.size());
		pageView.setQueryResult(qr);
		
#20160123
	定义错误编码字典类：
		整理一套通用的错误码集合，通过此错误码集合，能够快速定位问题；
			现阶段针对错误码进行 4 种类别的划分：  
				source-from 请求来源； 
				service-name 调用的service名称； 
				error-type 错误类别，大类别；
				error-info 错误类别，错误明细；
				
#20160125
	TreeNode 类 主键id 改为 Long 类型
	
#20160219
	mvn install:install-file -DgroupId=com.alexgaoyh -DartifactId=sap-jco -Dversion=1.0.0 -Dpackaging=jar -Dfile=D:\\test\\sapjco.jar

	mvn install:install-file -DgroupId=com.alexgaoyh -DartifactId=SAP_1.0.2 -Dversion=1.0.2 -Dpackaging=jar -Dfile=D:\\test\\SAP_1.0.2.jar

	mvn install:install-file -DgroupId=com.alexgaoyh -DartifactId=IS_Business_04_1.0.15 -Dversion=1.0.15 -Dpackaging=jar -Dfile=D:\\test\\IS_Business_04_1.0.15.jar
	
#20160301
	增加 File DataBase 操作，解决使用文件进行资源CRUD 的功能；
		FileDatabaseHelperTest 为单元测试，获取console控制台输入的信息，进行资源CRUD功能；
		
#20160302
	common模块：	增加一致性Hash 的简单实现；
		com.MutiModule.common.utils.hash.ConsistentHash 类文件；
	
#20160307
	common模块：	增加 java动态代理 的简单实现：
		src/test/java 下 com.MutiModule.common.proxy 包路径下；
			单元测试详见 DynamicTest.java 文件；
			
	common模块：	增加  对象集合转换为 属性结构的转换功能；
		/src/test/java 下 com.MutiModule.common.treeNode 包路径下；
			单元测试详见 ToTreeNodeTest.java 文件；
			
#20160310
	common模块：	修复 文件操作  com.MutiModule.common.utils.file.FileDatabaseHelper 类文件
		新增方法，根据Id获取实体； 移除 static 标识；
		
#20160311
	增加 org.json.json 包文件，增加将 xml 字符串 转换为 json 串 的方法；
	
#20160319
	增加 CFX 依赖，解决直接调用webservice 的单元测试 	com.MutiModule.common.soap.cfx.CFXWebServiceTest
	
#20160420
	common模块： 增加mybatis plugin 扩展插件（mybatis-generator）
			com.MutiModule.common.mybatis.plugin.SelectCountAndListByMapPlugin
				处理 selectCountByMap selectListByMap 两种方法；
			解决 mybatis-generator 插件中不生成 Example 类的方式；
			
#20160618
	IP 到 地址 映射库
		https://github.com/lionsoul2016/ip2region
		http://git.oschina.net/lionsoul/ip2region
		
#20160621
	增加BTree 的java 实现
		针对 Integer类，自定义类 进行测试，测试数据查询效率；
			目前针对  ip.merge.txt 文件进行测试，增加查询的效率测试，查询效率很快；
			后续如果针对大数据量的数据参数进行查找操作，可以采取这种做法；
			
	