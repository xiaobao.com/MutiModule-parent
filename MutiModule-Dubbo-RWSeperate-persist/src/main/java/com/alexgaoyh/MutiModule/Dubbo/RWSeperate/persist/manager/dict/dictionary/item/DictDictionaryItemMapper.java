package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItem;
import java.util.List;
import java.util.Map;

public interface DictDictionaryItemMapper {
    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<DictDictionaryItem> selectListByMap(Map<Object, Object> map);

    int insert(DictDictionaryItem record);

    int insertSelective(DictDictionaryItem record);

    DictDictionaryItem selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DictDictionaryItem record);

    int updateByPrimaryKey(DictDictionaryItem record);
    
    /**
     * 根据外键关联，将字典值部分数据全部删除
     * @param dictId
     * @return
     */
    int deleteByDictid(String dictId);
}