package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.upload;

import com.MutiModule.common.mybatis.base.BaseMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.upload.DemoAttachment;
import org.apache.ibatis.annotations.Param;;

public interface DemoAttachmentMapper extends BaseMapper<DemoAttachment> {
    int deleteByPrimaryKey(String id);

    DemoAttachment selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    DemoAttachment selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DemoAttachment record);

    int updateByPrimaryKey(DemoAttachment record);
}