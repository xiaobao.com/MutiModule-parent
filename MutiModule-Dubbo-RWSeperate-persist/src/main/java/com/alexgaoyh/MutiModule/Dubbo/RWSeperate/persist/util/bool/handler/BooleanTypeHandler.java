package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.util.bool.handler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

public class BooleanTypeHandler implements TypeHandler<Boolean> {

	/**
	 * 通过rs返回结果
	 */
	public Boolean getResult(ResultSet rs, String name) throws SQLException {
		String str = rs.getString(name);
		Boolean rt = Boolean.FALSE;
		if (str.equalsIgnoreCase("Y")) {
			rt = Boolean.TRUE;
		}
		return rt;
	}

	/**
	 * 通过CallableStatement 返回结果
	 */

	public Boolean getResult(CallableStatement statement, int index) throws SQLException {
		String str = statement.getString(index);
		if ("Y".equalsIgnoreCase(str))
			return true;
		else
			return false;
	}

	/**
	 * 设置字段
	 */
	public void setParameter(PreparedStatement pstatment, int index, Boolean bool, JdbcType type) throws SQLException {
		if (bool) {
			pstatment.setString(index, "Y");
		} else {
			pstatment.setString(index, "N");
		}
	}

	/**
	 * 通过rs返回是不是Y，是Y则返回true
	 */

	public Boolean getResult(ResultSet rs, int index) throws SQLException {

		String str = rs.getString(index);
		if ("Y".equalsIgnoreCase(str)) {
			return true;
		} else {
			return false;
		}
	}

}