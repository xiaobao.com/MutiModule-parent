package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course;

import java.util.List;
import java.util.Map;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.view.CourseStudentView;

public interface OneWithManyCourseMapper {
    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<OneWithManyCourse> selectListByMap(Map<Object, Object> map);

    int insert(OneWithManyCourse record);

    int insertSelective(OneWithManyCourse record);

    OneWithManyCourse selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(OneWithManyCourse record);

    int updateByPrimaryKey(OneWithManyCourse record);
    
	// alexgaoyh add
    /**
     * 根据course 的过滤规则，查询出 一个班级下的所有学生信息
     * @param example
     * @return
     */
    List<CourseStudentView> selectCourseWithStudentByExample(Map<Object, Object> map);
}