package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser.SysmanUser;
import java.util.List;
import java.util.Map;

public interface SysmanUserMapper {
	int deleteByPrimaryKey(String id);

	int selectCountByMap(Map<Object, Object> map);

	List<SysmanUser> selectListByMap(Map<Object, Object> map);

	int insert(SysmanUser record);

	int insertSelective(SysmanUser record);

	SysmanUser selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(SysmanUser record);

	int updateByPrimaryKey(SysmanUser record);
}