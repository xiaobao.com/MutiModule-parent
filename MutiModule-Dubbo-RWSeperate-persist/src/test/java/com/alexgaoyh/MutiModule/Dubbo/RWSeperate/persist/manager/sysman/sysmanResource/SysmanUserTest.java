package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser.SysmanUser;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser.SysmanUserMapper;

public class SysmanUserTest {

	private SysmanUserMapper mapper;

	// @Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (SysmanUserMapper) ctx.getBean( "sysmanUserMapper" );
        
    }
	
	// @Test
	public void selectByPrimaryKeyTest() {
		SysmanUser entity = mapper.selectByPrimaryKey("682814769132081152");
		System.out.println(entity.getCreateTime());
	}
}
