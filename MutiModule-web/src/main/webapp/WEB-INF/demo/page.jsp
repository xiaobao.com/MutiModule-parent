<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<head>

	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jqpagination/jqpagination.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery-v1.11.3.js"></script>
	<script charset="utf-8" src="${pageContext.request.contextPath}/js/jqpagination/jquery.jqpagination.js"></script>
	
</head>
<body>
	<c:forEach var="data" items="${pagination.data}" varStatus="status">
	${data.id }
	</c:forEach>
	
	<div class="gigantic pagination">
	    <a href="#" class="first" data-action="first">&laquo;</a>
	    <a href="#" class="previous" data-action="previous">&lsaquo;</a>
	    <input type="text" readonly="readonly" />
	    <a href="#" class="next" data-action="next">&rsaquo;</a>
	    <a href="#" class="last" data-action="last">&raquo;</a>
	</div>
	
	<script>
		var context_ = '${context_}';
		var currentPage = '${page.pageNo}';
		var maxPage = '${page.pageCount}';
	
		$(document).ready(function() {
			$('.pagination').jqPagination({
				current_page : currentPage,
				max_page : maxPage,
				paged : function(page) {
					window.location.href = context_ + '/demo/page/' + page;
				}
			});
		});
	</script>
</body>
</html>