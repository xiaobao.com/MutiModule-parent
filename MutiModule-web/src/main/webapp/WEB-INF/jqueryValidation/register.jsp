<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>注册register页面</title>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery-v1.11.3.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/validation/jquery.validate.js"></script>
	<style type="text/css">
	body {
		font-family: "宋体", Arial, Helvetica, sans-serif;
		color: #333333;
		font-size: 12px;
	}
	
	.reg {
		width: 450px;
		height: auto;
		margin: 0 auto;
		padding-top: 100px;
	}
	
	.topic {
		width: 450px;
		height: 54px;
	}
	
	.content {
		border: 1px solid #DBDBDB;
		color: #003976;
		background: #F9F9F9;
		text-align: center
	}
	
	.btn {
		color: #FFFFFF;
		width: 58px;
		height: 27px;
		border: 0; /*去除边框*/
	}
	</style>
</head>

<body>
	<div class="reg">
		<div class="topic"></div>
		<div class="content">
			<form id="commentForm" method="get">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="3" height="30"></td>
					</tr>
					<tr>
						<td width="120px">姓    名:</td>
						<td width="220px"><input id="cname" name="name" minlength="2" type="text" required /></td>
					</tr>
					<tr>
						<td colspan="3" height="10"></td>
					</tr>
					<tr>
						<td>联系方式:</td>
						<td><input type="text" /></td>
					</tr>
					<tr>
						<td colspan="3" height="10"></td>
					</tr>
					<tr>
						<td>Email:</td>
						<td><input id="cemail" type="email" name="email" required /></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="3" height="10"></td>
					</tr>
					<tr>
						<td>URL:</td>
						<td><input id="curl" type="url" name="url" /></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="3" height="10"></td>
					</tr>
					<tr>
						<td>Comments:</td>
						<td><textarea id="ccomment" name="comment" required></textarea></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="3" height="10"></td>
					</tr>
					<tr>
						<td colspan="2"><input class="submit" type="submit" value="Submit" /></td>

					</tr>

				</table>
			</form>

		</div>

	</div>
	<script type="text/javascript">
		$().ready(function() {
			$("#commentForm").validate({
				messages: {
					email: {
						required: 'Enter this!'
					}
				}
			});
		});
	</script>
</body>
</html>
