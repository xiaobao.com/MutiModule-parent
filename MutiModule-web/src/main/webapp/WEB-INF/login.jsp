<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin/index.css">
</head>
<body>

	<form id="login" action="${pageContext.request.contextPath}/sysmanUser/doLogin" method="post">
		<h1>Log In</h1>
		<fieldset id="inputs">
			<input name="userName" id="userName" type="text" placeholder="Username" autofocus required> 
			<input name="passWord" id="passWord"  type="password" placeholder="Password" required>
		</fieldset>
		<input type="checkbox" name="rememberMe" value="true" /> Remember me
		<fieldset id="actions">
			<input type="submit" id="submit" value="Log in"> 
		</fieldset>
	</form>
</body>
</html>
