/**
 * context 指定为 项目上下文
 * index 如果定义多组dataGrid，index指定为对应的参数：一组dataGrid包含（datagrid;toorbar;dialog;button）
 * templateUrl 指定为 此次访问操作对应的controller路径
 */
function DataGridBlankEasyui(context, index, templateUrl, pId) {
	this.context = context;
	this.index = index;
	this.templateUrl = templateUrl;
	this.pId = pId;
	this.saving =false; //处理中
};


$.extend(DataGridBlankEasyui.prototype, {
	
	/**
	 * 初始化DataGridEasyui
	 */
	init : function() {
		
		this.dataGrid = $("#dg-" + this.index);
		this.dlg = $("#dlg-" + this.index);
		this.toolBar = $("#toolbar-" + this.index);
		
		var fns = [ this.proxy(this.add, this,this.toolBar.find(".add")), 
		            this.proxy(this.edit, this,this.toolBar.find(".edit")),
					this.proxy(this.remove, this,this.toolBar.find(".remove")), 
					this.proxy(this.save, this,this.toolBar.find(".save")),
					this.proxy(this.update, this,this.toolBar.find(".update"))];

		this.toolBar.find(".add").bind('click', fns[0]);
		this.toolBar.find(".edit").bind('click', fns[1]);
		this.toolBar.find(".remove").bind('click', fns[2]);
		this.toolBar.find(".save").bind('click', fns[3]);
		this.toolBar.find(".update").bind('click', fns[4]);
		
		if(this.pId != undefined) {
			this.formGetData(this.pId);
		}
		
	},
	
	/**
	 * 改变函数作用域
	 * 
	 * @param fn
	 * @param scope
	 * @returns
	 */
	proxy : function(fn, scope,btn) {
		return function (){
			if(btn.is("[class*='disabled']")){  //禁用了不需要响应事件
				return ;
			}
			return fn.call(scope,arguments[0],btn);
		};
	},
	
	/**
	 * 初始化dialog里面的form表单
	 */
	formInit : function() {
		
	},
	
	/**
	 * toorbar的增加按钮
	 */
	add : function() {
		var this_ = this;
		window.location.href = context_ + "/" + this_.getController("add");
		/*this_.mainTabs.tabs('add', {
			title: '新增',
			content : this_.createFrame(url),
			closable : true
		});*/
		this.formInit.call(this);
	},
	
	/**
	 * toorbar的修改按钮
	 */
	edit : function() {
		var rows = this.dataGrid.datagrid('getSelections');
		if (!rows || rows.length == 0) {
			$.messager.alert('提示', '请选择记录！');
		} else {
			if (rows.length == 1) {
				var this_ = this;
				window.location.href = context_ + "/" + this_.getController("edit") + "/" + rows[0].id;
			} else {
				$.messager.alert('提示', '请选择单行记录！');
			}
		}
		this_.formLoadData(rows[0]);
	},
	
	/**
	 * toorbar的删除按钮
	 */
	remove : function() {
		var this_ = this;
		var rows = $('#dg-' + this.index).datagrid('getSelections');
		
		if (!rows || rows.length > 0) {
			$.messager.confirm('确认', '你确定要删除所选的记录吗?', function(r) {
				if (r) {
					$.post(context_ + "/" + this_.getController("logicDelete"), {
						ids : $.map(rows, function(row) {
							return row.id;
						}).join("::")

					}, function(result) {
						if (result.success) {
							this_.reload.call(this_);
							$.messager.show({ // show
								// tips
								title : '提示',
								msg : result.msg
							});
						} else {
							$.messager.alert('错误', result.msg);
						}
					}, 'json');
				}
			});
		} else {
			$.messager.alert('提示', '请选择记录！');
		}
	},
	
	/**
	 * form表单加载数据
	 */
	formLoadData:function (data){
		//处理隐藏域
		$("#dlg-" + this.index).find("form:eq(0)").each(function(){
			
			$(this).find("input").each(function() {
				var name =  $(this)[0].name;
				
				var value = data[name];
				if(value){
					$(this).val(value);
					return ;
				}
			});
			
		});
		
	},
	
	formGetData : function(editId) {
		var this_ = this;
		$.post(context_ + "/" + this.getController("selectByPrimaryKey") + "/" + editId, {

		}, function(result) {
			this_.formLoadData(result);
		}, 'json');
	},
	
	reload:function (){
		this.dataGrid.datagrid('reload'); // reload
	},
	
	/**
	 * form 表单验证
	 */
	validateForm:function (form){
		return true;
	},
	
	/**
	 * 保存按钮
	 */
	save : function() {
		if(this.saving==true){  //避免重复提交
			return ;
		}
		var this_ = this;
		var form = $("#dlg-" + this.index).find('form:eq(0)');
		
		form.form('submit', {
			url : context_ + "/" + this.getController("doSave"),
			onSubmit : function() {
				
				var validate = $(this).form('validate')&& this_.validateForm(form);
				
				if(validate){
					this_.saving = true;
				}
				
				return validate;
			},
			success : function(result) {
				this_.saving = false ;
				try{
					result = jQuery.parseJSON(result);
				}catch(e){
					$.messager.alert('错误', "服务端出错！"); // show error
					return ;
				}
				if (result.success) {
					this_.reload.call(this_);
					$.messager.show({ // show tips
						title : '提示',
						msg : result.msg
					});
				} else {
					$.messager.alert('错误', result.msg); // show error
				}
				window.history.go(-1);
			},
			onLoadError:function (){
				this_.saving = false;
			}
		});
	},
	
	/**
	 * 更新按钮
	 */
	update : function() {
		
		if(this.saving==true){  //避免重复提交
			return ;
		}
		var this_ = this;
		var form = $("#dlg-" + this.index).find('form:eq(0)');
		
		form.form('submit', {
			url : context_ + "/" + this.getController("doUpdate"),
			onSubmit : function() {
				
				var validate = $(this).form('validate')&& this_.validateForm(form);
				
				if(validate){
					this_.saving = true;
				}
				
				return validate;
			},
			success : function(result) {
				this_.saving = false ;
				try{
					result = jQuery.parseJSON(result);
				}catch(e){
					$.messager.alert('错误', "服务端出错！"); // show error
					return ;
				}
				if (result.success) {
					this_.reload.call(this_);
					$.messager.show({ // show tips
						title : '提示',
						msg : result.msg
					});
				} else {
					$.messager.alert('错误', result.msg); // show error
				}
				window.history.go(-1);
			},
			onLoadError:function (){
				this_.saving = false;
			}
		});
	},
	
	/**
	 * 获取响应方法
	 */
	getController : function(method) {
		return this.templateUrl + "/" + method;
	},
	
	/**
	 * 创建iframe
	 */
	createFrame : function(url) {
		var s = '<iframe name="mainFrame" scrolling="auto" frameborder="no" border="0" marginwidth="0" marginheight="0"  allowtransparency="yes" src="'
				+ url + '" style="width:100%;height:99%;"></iframe>';
		return s;
	}
	
});
