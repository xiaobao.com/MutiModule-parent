package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.data.authority.model;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.model.write.ISqlDataAuthorityOperationModelWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModel;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModelMapper;

@Service(value = "sqlDataAuthorityOperationModelService")
public class SqlDataAuthorityOperationModelServiceImpl implements ISqlDataAuthorityOperationModelWriteService{
	
	@Resource(name = "sqlDataAuthorityOperationModelMapper")
	private SqlDataAuthorityOperationModelMapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SqlDataAuthorityOperationModel record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SqlDataAuthorityOperationModel record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(SqlDataAuthorityOperationModel record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SqlDataAuthorityOperationModel record) {
		return mapper.updateByPrimaryKey(record);
	}
	
}
