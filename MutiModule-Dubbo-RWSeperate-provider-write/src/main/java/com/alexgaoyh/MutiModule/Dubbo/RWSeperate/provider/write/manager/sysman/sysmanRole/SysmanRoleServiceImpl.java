package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.sysman.sysmanRole;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.write.ISysmanRoleWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRole;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRoleMapper;

@Service(value = "sysmanRoleService")
public class SysmanRoleServiceImpl implements ISysmanRoleWriteService{
	
	@Resource(name = "sysmanRoleMapper")
	private SysmanRoleMapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysmanRole record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SysmanRole record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(SysmanRole record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysmanRole record) {
		return mapper.updateByPrimaryKey(record);
	}

}
