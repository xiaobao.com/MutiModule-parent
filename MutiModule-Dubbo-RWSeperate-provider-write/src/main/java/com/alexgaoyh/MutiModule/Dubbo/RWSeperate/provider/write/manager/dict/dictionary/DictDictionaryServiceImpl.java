package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.dict.dictionary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.dict.dictionary.write.IDictDictionaryWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionary;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionaryMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItem;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItemMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view.DictDictionaryItemWithOutPrefixIdView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view.DictDictionaryWithItemView;

@Service(value = "dictDictionaryService")
public class DictDictionaryServiceImpl implements IDictDictionaryWriteService{
	
	@Resource(name = "dictDictionaryMapper")
	private DictDictionaryMapper mapper;
	
	@Resource(name = "dictDictionaryItemMapper")
	private DictDictionaryItemMapper itemMapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(DictDictionary record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(DictDictionary record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(DictDictionary record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(DictDictionary record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public int insertDictDictionaryView(DictDictionaryWithItemView dictDictionaryView) {
		int returnInt = 1;
		DictDictionary dictDictionary = (DictDictionary)dictDictionaryView;
		mapper.insert(dictDictionary);
		List<DictDictionaryItemWithOutPrefixIdView> item = dictDictionaryView.getItems();
		for (int i = 0; i < item.size(); i++) {
			DictDictionaryItem _dDictionaryItem = (DictDictionaryItem)item.get(i);
			itemMapper.insert(_dDictionaryItem);
			returnInt++;
		}
		return returnInt;
	}

	@Override
	public int updateDictDictionaryView(DictDictionaryWithItemView dictDictionaryView) {
		int returnInt = 1;
		DictDictionary dictDictionary = (DictDictionary)dictDictionaryView;
		mapper.updateByPrimaryKeySelective(dictDictionary);
		
		//删除所有子数据表数据
		itemMapper.deleteByDictid(dictDictionary.getId());
		
		List<DictDictionaryItemWithOutPrefixIdView> item = dictDictionaryView.getItems();
		for (int i = 0; i < item.size(); i++) {
			DictDictionaryItem _dDictionaryItem = (DictDictionaryItem)item.get(i);
			itemMapper.insert(_dDictionaryItem);
			returnInt++;
		}
		return returnInt;
	}
	
}
