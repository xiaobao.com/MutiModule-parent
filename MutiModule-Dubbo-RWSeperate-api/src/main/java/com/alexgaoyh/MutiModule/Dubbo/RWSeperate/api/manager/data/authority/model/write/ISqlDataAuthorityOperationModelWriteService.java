package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.model.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModel;

/**
 * SqlDataAuthorityOperationModel 模块 写接口
 * @author alexgaoyh
 *
 */
public interface ISqlDataAuthorityOperationModelWriteService {

	int deleteByPrimaryKey(String id);

	int insert(SqlDataAuthorityOperationModel record);

	int insertSelective(SqlDataAuthorityOperationModel record);

	int updateByPrimaryKeySelective(SqlDataAuthorityOperationModel record);

	int updateByPrimaryKey(SqlDataAuthorityOperationModel record);
}
