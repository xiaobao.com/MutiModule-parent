package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRoleResourceRel.write;

import java.util.List;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel.SysmanRoleResourceRelKey;

/**
 * SysmanRoleResourceRel 模块 写接口
 * @author alexgaoyh
 *
 */
public interface ISysmanRoleResourceRelWriteService {

	int deleteByPrimaryKey(SysmanRoleResourceRelKey key);

	int insert(SysmanRoleResourceRelKey record);

	int insertSelective(SysmanRoleResourceRelKey record);

	int insertList(List<SysmanRoleResourceRelKey> list);    
    /**
     * 根据 角色id ，删除此角色下所有资源
     * @param sysmanRoleId	角色id
     * @return
     */
    int deleteBySysmanRoleId(String sysmanRoleId);
    
    /**
     * 刷新用户角色与用户资源的关联关系
     * @param sysmanRoleId	用户角色id
     * @param sysmanResourceIds	用户资源id集合，以逗号为分隔符
     * @return
     */
    int refreshSysmanRoleResourceRel(String sysmanRoleId, String sysmanResourceIds);
}
