package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRole;

/**
 * SysmanRole 模块 写接口
 * @author alexgaoyh
 *
 */
public interface ISysmanRoleWriteService {

	int deleteByPrimaryKey(String id);

    int insert(SysmanRole record);

    int insertSelective(SysmanRole record);

    int updateByPrimaryKeySelective(SysmanRole record);

    int updateByPrimaryKey(SysmanRole record);
}
