package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.group.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group.SqlDataAuthorityOperationGroup;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationGroupWithRuleView;

/**
 * SqlDataAuthorityOperationGroup 模块 写接口
 * @author alexgaoyh
 *
 */
public interface ISqlDataAuthorityOperationGroupWriteService {

	int deleteByPrimaryKey(String id);

	int insert(SqlDataAuthorityOperationGroup record);

	int insertSelective(SqlDataAuthorityOperationGroup record);

	int updateByPrimaryKeySelective(SqlDataAuthorityOperationGroup record);

	int updateByPrimaryKey(SqlDataAuthorityOperationGroup record);
	
	// alexgaoyh add
	
	/**
	 * 保存 数据权限 组别 的实体信息部分 
	 * @param groupWithRuleView
	 * @return
	 */
	int insertOperationGroupWithRuleView(SqlDataAuthorityOperationGroupWithRuleView groupWithRuleView);
	
	/**
	 * 保存 数据权限 组别 的实体信息部分 
	 * @param record	实体entity
	 * @param ruleIds	包含的 数据权限 规则的ids 集合
	 * @return
	 */
	int insertOperationGroupWithRuleView(SqlDataAuthorityOperationGroup record, String[] ruleIds);
}
