#20151111
	MutiModule-Dubbo-RWSeperate-api 模块的创建，此模块可以理解为是 业务层的api接口模块；
		之所以将这一部分 api接口部分抽离出来，是为了在分布式系统的调用过程中，减少重复代码量的编写；
			同时注意这一部分的api接口，针对同一个业务模块，是分为 读接口 写接口 两个的，这样在不同的业务层调用过程中，能够区分读写分离相关的业务逻辑操作；
		
		同时注意这一部分模块依赖于 MutiModule-Dubbo-RWSeperate-persist 模块，依赖于persist模块中的部分entity实体信息；
			之后两个服务提供者的serviceimpl 部分，再依赖于这个api模块
			
			三个依赖模块如下图所示： provider服务提供者模块依赖 api 模块，api模块依赖persist层的模块；
				------provider
					|-----Api
						|-----persist
					
#20160308
	Dubbo-consumer Dubbo-provider-read Dubbo-provider-write 三个模块：
		抽离 配置文件 xml 内部的 
			<dubbo:registry protocol="zookeeper" address="192.168.2.211:2181" />
	部分 到 Dubbo-api 模块中，减少重复代码；一个地方修改即可；
	
#20160309
	Dubbo-api 模块 ：
		此模块下配置了 dubbo-register 的 address 部分，可以将这个地址修改为本地地址；
			从官网下载 zookeeper ，解压;
				 windows环境对应zkServer.cmd	
				 linux 环境对应  zkServer.sh 
			修改解压目录的 conf 文件夹下的 zoo_sample.cfg ，改名为 zoo.cfg， 注意此文件夹下的部分配置，防止配置部分错误（port）造成启动失败；
			F:\zookeeper-3.4.8\zookeeper-3.4.8\bin>zkServer.cmd  	即可启动zookeeper （windows环境） 
			
#20160421
	Dubbo-RWSeperate-* 模块：
			将 sys_*  后台 RABC 相关的表结构都进行处理，移除 Example 相关的表结构，改为自定义的通用方法；
				本地扩展mybatis-generator 插件，自定义插件解决基础功能处理；
				后续有新的业务逻辑，则重新扩展此自定义插件；