#20151203
	创建 baidu ECharts 相关测试demo
	
#2015124
	创建Echarts与WebSocket的关联关系；
		echarts在初始化到页面的过程中，客户端（浏览器）直接与服务端创建长连接，后期服务端主动向客户端发送消息，echart页面则开始刷新；
			Demo:
				http://127.0.0.1:8080/MutiModule-echarts/init 初始化echart页面，页面中展现一个图标信息
				另开一个页面访问 http://127.0.0.1:8080/MutiModule-echarts/send 服务器主动向客户端发送消息，则看到之前init链接对应的页面内容发生了变化；
				
	至此，echarts与WebSocket之间则解决了交互问题，并且可以通过服务端控制客户端页面的展现内容相关；
	
#20151217
	servlet 3.0+ 支持 
		<async-supported>true</async-supported> 配置
		
#20160117
	增加 echarts 地图map省市区数量颜色划分与地理位置markLine标线划分；
		增加js setInterval 定时器，动态调用 myChart.addMarkLine() 增加标线
		
#20160118
	增加echarts 地图省市区颜色划分(数据更新)
		如果markLine标线的目的地为地图上标注的点(现阶段为省份名称)，则实时更新订单数(地图上标注的省市区的订单数数量)；