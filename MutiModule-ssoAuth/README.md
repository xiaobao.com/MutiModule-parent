#20150723
	SSO單點登陸： 登陸校驗模塊， 詳見 web.xml 的配置，並且他需要 common模塊的sso.properties 文件 配合支持
	
#20150803
	抽离出来新的 MutiModule-static 模块，用来保存通用的 静态资源文件(js/css/image……)：
		1： 后期如果做动静分离的过程中，这一部分静态资源可以直接抽离出来，不需要改动太多；
		2： “浏览器对于同一域名的并发获取（加载）资源数是有限的”， 这与，保证静态资源相关都保存在 另外的一个域名下；
		3： 本地测试过程中，
			3.1：需要读取common模块中配置的StaticUrl.properties 配置文件，
			3.2：并且在servlet部分，需要将配置的staticUrl 存入 HttpServletRequest， 
			3.3： 页面读取相关staticUrl，并且读取静态资源static模块下的静态资源文件