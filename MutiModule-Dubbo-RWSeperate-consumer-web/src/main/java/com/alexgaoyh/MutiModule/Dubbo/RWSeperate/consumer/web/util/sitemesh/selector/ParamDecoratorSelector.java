package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.util.sitemesh.selector;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.sitemesh.DecoratorSelector;
import org.sitemesh.content.Content;
import org.sitemesh.webapp.WebAppContext;

import com.MutiModule.common.utils.StringUtilss;

public class ParamDecoratorSelector implements DecoratorSelector<WebAppContext> {

	private DecoratorSelector<WebAppContext> defaultDecoratorSelector;

	public ParamDecoratorSelector(DecoratorSelector<WebAppContext> defaultDecoratorSelector) {
		this.defaultDecoratorSelector = defaultDecoratorSelector;
	}

	@Override
	public String[] selectDecoratorPaths(Content content, WebAppContext context) throws IOException {
		HttpServletRequest request = context.getRequest();
		String decorator = request.getParameter("decorator");
		if (StringUtilss.isNotBlank(decorator) && decorator.equals("true")) {
			return new String[] {};
		}
		// Otherwise, fallback to the standard configuration
		return defaultDecoratorSelector.selectDecoratorPaths(content, context);
	}

}
