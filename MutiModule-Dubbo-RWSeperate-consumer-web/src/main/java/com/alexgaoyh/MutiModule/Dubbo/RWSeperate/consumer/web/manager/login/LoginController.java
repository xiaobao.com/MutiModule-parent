package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.login;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.utils.CookieUtilss;
import com.MutiModule.common.utils.DubboRWSepConWebPropUtilss;
import com.MutiModule.common.utils.StringUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read.ISysmanResourceReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUser.read.ISysmanUserReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser.SysmanUser;
import com.alibaba.dubbo.config.annotation.Reference;

@Controller
@RequestMapping(value = "")
public class LoginController {
	
	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read.ISysmanResourceReadService")
	private ISysmanResourceReadService sysmanResourceReadService;
	
	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUser.read.ISysmanUserReadService")
	private ISysmanUserReadService sysmanUserReadService;

	/**
	 * 首页
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/")
	public ModelAndView login(ModelAndView model, HttpServletRequest request) {
		model.setViewName("login");
		return model;
	}
	
	/**
	 * 登陆校验方法
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/toLogin")
	public ModelAndView toLogin(ModelAndView model, HttpServletRequest request, HttpServletResponse response) {
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String redirect = request.getParameter("redirect");
		
		if(StringUtilss.isNotEmpty(userName) && StringUtilss.isNotEmpty(password)) {
			
			Map<Object, Object> map = new HashMap<Object, Object>();
			map.put("userName", userName);
			map.put("password", password);
			List<SysmanUser> list = sysmanUserReadService.selectListByMap(map);
			
			if(list.size() > 0) {
				//登陆成功
				CookieUtilss.add(response, DubboRWSepConWebPropUtilss.getAdminCookie(), list.get(0).getId().toString(), 30*30*60, "/");
				
				if(StringUtilss.isNotEmpty(redirect)) {
					try {
						redirect = URLDecoder.decode(redirect, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						redirect = "/manager/index";
						e.printStackTrace();
					}
					model.setViewName("redirect:" + redirect);
				} else {
					model.setViewName("redirect:/manager/index");
				}
				
			} else {
				//登陆失败
				model.setViewName("redirect:/");
			}
		} else {
			//缺失参数
			model.setViewName("redirect:/error/10006");
		}
		
		return model;
	}
	
	@RequestMapping(value = "/logOut")
	public ModelAndView logOut(ModelAndView model, HttpServletRequest request, HttpServletResponse response) {
		
		CookieUtilss.removeAll(request, response, "/");
		
		model.setViewName("redirect:/");
		return model;
	}
}
