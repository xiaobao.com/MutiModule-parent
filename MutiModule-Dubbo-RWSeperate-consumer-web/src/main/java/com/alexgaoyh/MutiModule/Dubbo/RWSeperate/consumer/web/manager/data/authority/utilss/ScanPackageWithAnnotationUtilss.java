package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.data.authority.utilss;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.MutiModule.common.mybatis.annotation.MyBatisColumnAnnotation;
import com.MutiModule.common.mybatis.annotation.MyBatisTableAnnotation;
import com.MutiModule.common.utils.ClassUtilss;
import com.MutiModule.common.utils.StringUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.vo.SqlDataAuthorityModelVO;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.vo.SqlDataAuthorityRuleVO;

public class ScanPackageWithAnnotationUtilss {

	/**
	 * 返回所有 包下注释的，包含所有指定注解的类的信息
	 * 
	 * @return
	 */
	public static final List<SqlDataAuthorityRuleVO> scanPackageWithAnnotation(String scanPackagePath) {
		if (StringUtilss.isEmpty(scanPackagePath)) {
			scanPackagePath = "com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist";
		}
		List<SqlDataAuthorityRuleVO> _ruleVOlist = new ArrayList<SqlDataAuthorityRuleVO>();

		Set<Class<?>> classSet = ClassUtilss.scanPackageByAnnotation(scanPackagePath, MyBatisTableAnnotation.class);
		for (Class<?> class1 : classSet) {
			MyBatisTableAnnotation annotationTable = class1.getAnnotation(MyBatisTableAnnotation.class);
			if (annotationTable == null) {
				continue;
			} else {
				SqlDataAuthorityRuleVO _ruleVO = new SqlDataAuthorityRuleVO(class1.getName(),
						annotationTable.remarks());

				Field[] field = class1.getDeclaredFields();

				if (field != null && field.length > 0) {

					List<SqlDataAuthorityModelVO> _modelVOList = new ArrayList<SqlDataAuthorityModelVO>();

					for (Field field2 : field) {
						MyBatisColumnAnnotation annotationColumn = field2.getAnnotation(MyBatisColumnAnnotation.class);
						if (annotationColumn != null) {
							SqlDataAuthorityModelVO _modelVO = new SqlDataAuthorityModelVO(field2.getName(),
									annotationColumn.chineseNote());
							_modelVOList.add(_modelVO);
						}
					}
					_ruleVO.setItems(_modelVOList);
				}
				_ruleVOlist.add(_ruleVO);
			}
		}
		return _ruleVOlist;
	}

}
