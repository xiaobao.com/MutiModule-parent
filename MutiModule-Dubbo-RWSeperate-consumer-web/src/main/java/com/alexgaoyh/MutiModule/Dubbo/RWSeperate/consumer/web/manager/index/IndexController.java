package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.index;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseEmptyController;

@Controller
@RequestMapping(value = "")
public class IndexController extends BaseEmptyController{
	
	@RequestMapping(value = "/manager/index")
	public ModelAndView manager(ModelAndView model, HttpServletRequest request) {
		
		model.setViewName("manager/index");
		return model;
	}
}
