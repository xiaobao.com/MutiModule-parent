<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE HTML>
	<table>
		<c:set var="index" value="0" scope="request" /><!-- 自增序号，注意scope-->  
		<c:set var="level" value="0" scope="request" /><!-- 记录树的层次，注意scope-->
		<c:import url="_r.jsp" />  
	</table>
</html>