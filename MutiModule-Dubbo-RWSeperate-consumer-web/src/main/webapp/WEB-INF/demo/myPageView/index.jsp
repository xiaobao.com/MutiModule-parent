<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
	<body>
		<table style="border: solid 1px green;">
			<tr>
				<td>msg内容</td>
			</tr>
			<c:forEach items="${pageView.records}" var="entry">
				<tr>
					<td>${entry.msg}</td>
				</tr>
			</c:forEach>
		</table>
		<!-- 将分页JSP包含进来 -->  
		<%@ include file="../../common/dataTable/myPageJSP.jsp" %>  
	</body>
</html>