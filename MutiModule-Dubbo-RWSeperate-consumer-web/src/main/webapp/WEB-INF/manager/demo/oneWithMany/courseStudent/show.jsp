<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>
					All form elements <small>With custom checbox and radion
						elements.</small>
				</h5>
			</div>
			<div class="ibox-content">
				<form method="post" class="form-horizontal" id="formHTML" action="${context_ }/${moduleName }/doSave">
					<div class="form-group">
						<label class="col-sm-2 control-label">班级名称</label>
						<div class="col-sm-10">
							<input type="text" id="name" name="name" value="${entity.name }"
								class="form-control">
						</div>
					</div>			
					<div class="hr-line-dashed"></div>
					<div class="row">
			            <div class="col-lg-12">
			            	<div class="ibox float-e-margins">
			            		<div class="ibox-content">
						            <table class="table table-striped table-bordered table-hover " id="editable" >
							            <thead>
							            <tr>
							                <th>学生姓名</th>
							            </tr>
							            </thead>
							            <tbody>
							            	<c:forEach var="student" items="${entity.studentList}" varStatus="vs">
							            		<c:if test = "${student.id != null}">
								            		<tr>
														<td><input type="text" id="studentList[${vs.index }].name" name="studentList[${vs.index }].name" value="${student.name }"  /></td>
													</tr>
												</c:if>
							            	</c:forEach>
							            </tbody>
							            <tfoot>
							            <tr>
							                <th>学生姓名</th>
							            </tr>
							            </tfoot>
            						</table>
          						</div>
				            </div>
			            </div>
			        </div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button class="btn btn-white" type="submit"
								onclick="javaScript:history.go(-1);return false;">返回</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$( document ).ready(function() {
			disableForm('formHTML', true);
		});
		
		//禁用form表单中所有的input[文本框、复选框、单选框],select[下拉选],多行文本框[textarea]
		function disableForm(formId,isDisabled) {
		    
		    var attr="disable";
			if(!isDisabled){
			   attr="enable";
			}
			$("form[id='"+formId+"'] :text").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] :password").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] textarea").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] select").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] :radio").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] :checkbox").attr("disabled",isDisabled);
		}
	</script>
</body>
</html>