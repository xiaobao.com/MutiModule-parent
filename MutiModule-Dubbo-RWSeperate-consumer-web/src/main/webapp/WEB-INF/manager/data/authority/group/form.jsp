<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<input type="hidden" id="id" name="id" value="${entity.id }"></input>
<div class="form-group">
	<label class="col-sm-2 control-label">名称</label>
	<div class="col-sm-10">
		<input type="text" id="name" name="name" value="${entity.name }"
			class="form-control">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">描述</label>
	<div class="col-sm-10">
		<input type="text" id="remarks" name="remarks" value="${entity.remarks }"
			class="form-control">
	</div>
</div>