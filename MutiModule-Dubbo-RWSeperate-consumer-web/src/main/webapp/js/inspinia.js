// Custom scripts
$(document).ready(function () {

    // MetsiMenu
    $('#side-menu').metisMenu();
    
    if(leftMenuId != undefined) {
    	var _leftMenuIdArray = leftMenuId.split('-');
    	$('#li-' + _leftMenuIdArray[0] + ' .collapse').removeClass("collapse" );
    	$('#li-' + _leftMenuIdArray[0] + ' ul').each(function() {
    		if($(this).attr("id").indexOf('-' + _leftMenuIdArray[0] + '-' + _leftMenuIdArray[1]) < 0) {
    			if($(this).attr("id").indexOf('third') > 0) {
    				$(this).addClass("collapse");
    			}
    		}
    	});
    	$('#li-' + _leftMenuIdArray[0]).addClass("active");
    	$('#li-nav-second-' + _leftMenuIdArray[0] + '-' + _leftMenuIdArray[1]).addClass("active");
    	$('#ul-nav-second-' + _leftMenuIdArray[0] + '-' + _leftMenuIdArray[1]).addClass("collapse in");
    	$('#ul-nav-third-' + _leftMenuIdArray[0] + '-' + _leftMenuIdArray[1]).attr('aria-expanded', true);
    	$('#ul-nav-third-' + _leftMenuIdArray[0] + '-' + _leftMenuIdArray[1]).addClass("collapse in");
    }
    
    //expand all submenus	针对li标签，打开这个li标签下的所有子菜单 ： 父集根节点 1010 为父节点id
    //针对  class="my-tree-li" 的这个li标签 ，打开它级别下的所有子菜单
    //$('#li-1010 .collapse').removeClass( "collapse" );
 
    // Collapse ibox function
    /*$('.collapse-link:not(.binded)').addClass("binded").click( function() {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });*/
 
    // Close ibox function
    /*$('.close-link:not(.binded)').addClass("binded").click( function() {
        var content = $(this).closest('div.ibox');
        content.remove();
    });*/
 
    // Small todo handler
    /*$('.check-link:not(.binded)').addClass("binded").click( function(){
        var button = $(this).find('i');
        var label = $(this).next('span');
        button.toggleClass('fa-check-square').toggleClass('fa-square-o');
        label.toggleClass('todo-completed');
        return false;
    });*/
 
    // Append config box / Only for demo purpose
    /*
    $.get("skin-config.html", function (data) {
        $('body').append(data);
    });
    */
 
    // minimalize menu
    $('.navbar-minimalize:not(.binded)').addClass("binded").click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    })
 
    // tooltips
    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })
 
    // Move modal to body
    // Fix Bootstrap backdrop issu with animation.css
    $('.modal').appendTo("body")
 
    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");
    }
    fix_height();
 
    // Fixed Sidebar
    // unComment this only whe you have a fixed-sidebar
            //    $(window).bind("load", function() {
            //        if($("body").hasClass('fixed-sidebar')) {
            //            $('.sidebar-collapse').slimScroll({
            //                height: '100%',
            //                railOpacity: 0.9,
            //            });
            //        }
            //    })
 
    $(window).bind("load resize click scroll", function() {
        if(!$("body").hasClass('body-small')) {
            fix_height();
        }
    })
 
    $("[data-toggle=popover]")
        .popover();
});


// For demo purpose - animation css script
window.animationHover = function(element, animation){
    element = $(element);
    element.hover(
        function() {
            element.addClass('animated ' + animation);
        },
        function(){
            //wait for animation to finish before removing classes
            window.setTimeout( function(){
                element.removeClass('animated ' + animation);
            }, 2000);
        });
};

// Minimalize menu when screen is less than 768px
$(function() {
    $(window).bind("load resize", function() {
        if ($(this).width() < 769) {
            $('body').addClass('body-small')
        } else {
            $('body').removeClass('body-small')
        }
    })
})

window.SmoothlyMenu = function() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 100);
    } else if ($('body').hasClass('fixed-sidebar')){
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 300);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
};

// Dragable panels
window.WinMove = function() {
    var element = "[class*=col]";
    var handle = ".ibox-title";
    var connect = "[class*=col]";
    $(element).sortable(
        {
            handle: handle,
            connectWith: connect,
            tolerance: 'pointer',
            forcePlaceholderSize: true,
            opacity: 0.8,
        })
        .disableSelection();
};

/**
 * 封装form表单进行数据提交 
 * @param url	链接
 * @param args	参数
 */
function StandardPost(url,args){
    var form = $("<form method='post'></form>"),
        input;
    form.attr({"action":url});
    $.each(args,function(key,value){
        input = $("<input type='hidden'>");
        input.attr({"name":key});
        input.val(value);
        form.append(input);
    });
    form.submit();
};

/**
 * 日期转换
 * @param date
 * @returns
 */
function parseDate(date){
	if(date == null) {
		return null;
	} else {
		return timeStamp2String(date);
	}
}

function timeStamp2String(time){  
    var datetime = new Date();  
    datetime.setTime(time);  
    var year = datetime.getFullYear();  
    var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;  
    var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();  
    var hour = datetime.getHours()< 10 ? "0" + datetime.getHours() : datetime.getHours();  
    var minute = datetime.getMinutes()< 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();  
    var second = datetime.getSeconds()< 10 ? "0" + datetime.getSeconds() : datetime.getSeconds();  
    return year + "-" + month + "-" + date+" "+hour+":"+minute+":"+second;  
} 

/**
 * 获取url请求链接中 ? 后面的参数部分
 * @returns {Object}
 */
function requestParams(name) {
   var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)","i");
   var r = window.location.search.substr(1).match(reg);
   if (r!=null) return (r[2]); return null;
}

$.fn.stringify = function() {
  return JSON.stringify(this);
}