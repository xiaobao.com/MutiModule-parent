package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.${packageName};

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.twitter.IDGenerator.instance.IdWorkerInstance;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.${packageName}.read.I${className}ReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.${packageName}.write.I${className}WriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.${packageName}.${className};
import com.alibaba.dubbo.config.annotation.Reference;

/**
*
* dubbo 消费者
 * ${className} 模块 读接口
 * @author alexgaoyh
*
*/
@Controller
@RequestMapping(value="${packageName?replace(".", "/")}")
public class ${className}Controller extends BaseController<${className}>{

	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.${packageName}.read.I${className}ReadService")
	private I${className}ReadService readService;
	
	@Reference(group="writeService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.${packageName}.write.I${className}WriteService")
	private I${className}WriteService writeService;
	
	

}
