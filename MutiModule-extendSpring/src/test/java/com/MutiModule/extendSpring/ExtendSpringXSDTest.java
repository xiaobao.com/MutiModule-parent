package com.MutiModule.extendSpring;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ExtendSpringXSDTest {

	@Test
	public void testExtendSpring() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("application.xml");  
		People p = (People)ctx.getBean("cutesource");  
		System.out.println(p.getId());  
		System.out.println(p.getName());  
		System.out.println(p.getAge());
	}
}
