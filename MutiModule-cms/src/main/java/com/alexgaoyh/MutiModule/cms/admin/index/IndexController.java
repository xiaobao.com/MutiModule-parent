package com.alexgaoyh.MutiModule.cms.admin.index;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.vo.TreeNode;
import com.alexgaoyh.MutiModule.service.sysman.ISysmanResourceService;

/**
 * 首頁 相關
 * @author alexgaoyh
 *
 */
@Controller
@RequestMapping(value="")
public class IndexController {
	
	@Resource
	private ISysmanResourceService sysmanResourceService;

	/**
	 * 首页
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView list(ModelAndView model) {
		
		List<TreeNode> treeNodeList = sysmanResourceService.selectTreeNodeBySysmanResourceParentId(87);
		model.addObject("treeNodeList", treeNodeList);
		
		model.setViewName("/admin/index");
		return model;
	}
}
