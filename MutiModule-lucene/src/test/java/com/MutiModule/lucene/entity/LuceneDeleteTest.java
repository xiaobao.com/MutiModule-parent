package com.MutiModule.lucene.entity;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.store.FSDirectory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.lucene.LuceneUtils;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocation;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocationExample;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocationMapper;

/**
 * 创建文件之后进行删除操作，之后新增方法 isDeletedIndex 用来判断是否成功删除索引文件
 * @author lenovo
 *
 */
public class LuceneDeleteTest {


	private BaseLocationMapper mapper;
	
	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (BaseLocationMapper) ctx.getBean( "baseLocationMapper" );
        
    }

	//@Test
	public void testAll() throws IOException, ParseException, InvalidTokenOffsetsException {
		createIndex();

		isDeletedIndex();
	}
	
	/**
	 * 生成索引文件
	 * @throws IOException
	 * @throws ParseException 
	 */
	public void createIndex() throws IOException, ParseException {
		
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
		// 配置索引
		IndexWriterConfig writerConfig = new IndexWriterConfig(LuceneUtils.analyzer);
        
		IndexWriter iWriter = LuceneUtils.getIndexWrtier(directory, writerConfig);
        
		BaseLocationExample example = new BaseLocationExample();
		example.or().andIdLessThan(201533);
		List<BaseLocation> list = mapper.selectByExample(example);
		
		for(BaseLocation bl : list) {
			Document doc = new Document();
			
			doc.add(new Field("id", bl.getId().toString(), TextField.TYPE_STORED));
			doc.add(new Field("createTime", bl.getCreateTime().toString(), TextField.TYPE_STORED));
			doc.add(new Field("nameCN", bl.getNameCN(), TextField.TYPE_STORED));
			
			iWriter.addDocument(doc);
		}
		
		// 使用同样的方式对多field进行搜索
        String[] multiFields = { "id"};
        MultiFieldQueryParser parser = new MultiFieldQueryParser(multiFields, LuceneUtils.analyzer);
        // 设定具体的搜索词
        Query query = parser.parse("10000");
        
        LuceneUtils.deleteIndex(iWriter, query);
        
        LuceneUtils.closeIndexWriter(iWriter);
		LuceneUtils.closeDirectory(directory);
	}
	
	/**
	 * 是否删除index文件
	 * @throws ParseException
	 * @throws IOException
	 */
	public void isDeletedIndex() throws ParseException, IOException {
		
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
        
		IndexReader iReader = LuceneUtils.getIndexReader(directory);
		
		IndexSearcher iSearcher = LuceneUtils.getIndexSearcher(iReader);
		
		// 使用同样的方式对多field进行搜索
        String[] multiFields = { "id"};
        MultiFieldQueryParser parser = new MultiFieldQueryParser(multiFields, LuceneUtils.analyzer);
        // 设定具体的搜索词
        Query query = parser.parse("10000");
        
        List<Document> list = LuceneUtils.query(iSearcher, query);
        System.out.println("list = " + list);
        for(Document doc : list) {
        	System.out.println(doc.get("nameCN"));
        }
        
        LuceneUtils.closeIndexReader(iReader);
		LuceneUtils.closeDirectory(directory);

	}
}
