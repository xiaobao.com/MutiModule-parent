package com.MutiModule.lucene.entity;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

import com.MutiModule.lucene.LuceneUtils;

/**
 * 索引更新操作
 * @author alexgaoyh
 *
 */
public class LuceneUpdateTest {
	
	//@Test
	public void testAll() throws IOException, ParseException, InvalidTokenOffsetsException {
		updateIndexTest();

		isUpdatedTest();
	}
	
	/**
	 * 更新索引文件
	 * @throws IOException
	 */
	public void updateIndexTest() throws IOException {
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
		// 配置索引
		IndexWriterConfig writerConfig = new IndexWriterConfig(LuceneUtils.analyzer);
        
		IndexWriter iWriter = LuceneUtils.getIndexWrtier(directory, writerConfig);
		
		Document doc = new Document();
		doc.add(new Field("id", "10000", TextField.TYPE_STORED));
		doc.add(new Field("createTime", new Date().toString(), TextField.TYPE_STORED));
		doc.add(new Field("nameCN", "test", TextField.TYPE_STORED));
		
		iWriter.updateDocument(new Term("id","10000"), doc);
		
		LuceneUtils.closeIndexWriter(iWriter);
	}
	
	/**
	 * 检查索引文件是否更新成功
	 * @throws ParseException
	 * @throws IOException 
	 */
	public void isUpdatedTest() throws ParseException, IOException {
		
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
		
		IndexReader iReader = LuceneUtils.getIndexReader(directory);
		
		IndexSearcher iSearcher = LuceneUtils.getIndexSearcher(iReader);
		
		// 使用同样的方式对多field进行搜索
        String[] multiFields = { "id"};
        MultiFieldQueryParser parser = new MultiFieldQueryParser(multiFields, LuceneUtils.analyzer);
        // 设定具体的搜索词
        Query query = parser.parse("10000");
        
        List<Document> list = LuceneUtils.query(iSearcher, query);
        for(Document doc : list) {
        	System.out.println(doc.get("nameCN"));
        }
        
        LuceneUtils.closeIndexReader(iReader);
		LuceneUtils.closeDirectory(directory);
	}
}
